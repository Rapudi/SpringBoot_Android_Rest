package com.spring.boot.android;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;



public class AndroidHome extends Activity {

    /**
     * Called when the activity is first created.
     * @param savedInstanceState If the activity is being re-initialized after 
     * previously being shut down then this Bundle contains the data it most 
     * recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // new HttpRequestTask().execute();
    }
    @Override
    protected void onStart() {
        super.onStart();
        new HttpRequestTask().execute();
    }

    private class HttpRequestTask extends AsyncTask<Void, Void, Customer> {
        @Override
        protected Customer doInBackground(Void... params) {
            try {
               // final String url = "https://myrestservicep1941345437trial.hanatrial.ondemand.com/myrestservice-0.0.1-SNAPSHOT/customer/get/6";
           
         
                RestTemplate restTemplate = new RestTemplate();
               
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Customer customer = restTemplate.getForObject("https://myrestservicep1941345437trial.hanatrial.ondemand.com/myrestservice-0.0.1-SNAPSHOT/customer/get/8", Customer.class);
                Log.d("CUSTOMER NAME", customer.getLname());
                return customer;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
                return null;
            }

           // return null;
        }
        @Override
        protected void onPostExecute(Customer customer) {
            TextView cusName = (TextView) findViewById(R.id.txtName);
            TextView cusSurname = (TextView) findViewById(R.id.txtSurname);
            TextView cusAddress = (TextView) findViewById(R.id.txtAddress);
            cusName.setText(customer.getFname());
            cusSurname.setText(customer.getLname());
            cusAddress.setText(customer.getAddress());
           
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(com.spring.boot.android.R.menu.main, menu);
	return true;
    }

}

